# Compilateur Basique VSL+ vers LLVM
TP2 de PDS
___
Maxime Cauté & Rémi Piau

## Compilation du projet

Pour compiler le projet utiliser la commande: `./gradlew build`

## Compilation d'un programme VSL

Pour compiler un programme VSL utiliser la commande suivante:
`./compile <program.vsl>`

## Réaliser les tests

Pour tester le compilateur un jeu de test est disponible
avec plusieurs niveaux dans le dossier `tests/`
(les niveaux 0 et 0-5 ne sont présents qu'à des fin de
développement et ne fonctionnent donc pas avec le compilateur).

On peut lancer un test en utilisant
`./test <chemin_vers_test_vsl>`
ou lancer tout les tests d'un dossier grace à
`./circletests <chemin_vers_le_dossier_contenant_les_tests>`

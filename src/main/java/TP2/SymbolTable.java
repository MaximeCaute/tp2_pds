package TP2;

import java.util.Map;

import TP2.ASD.Type;

import java.util.HashMap;
import java.util.Collection;
import java.util.Set;
import java.util.List;

import TP2.Utils.Log;

// This file contains the symbol table definition.
// A symbol table contains a set of ident and the
// corresponding symbols.
// It can have a parent, containing itself other
// symbols. If a symbol is not found, the request
// is forwarded to the parent.

public class SymbolTable {
  // Define different symbols
  public static abstract class Symbol {
    public String ident; // minimum, used in the storage map
    public Type type;
  }

  public static class VariableSymbol extends Symbol {

    public VariableSymbol(Type type, String ident) {
      this.type = type;
      this.ident = ident;
    }

    @Override public boolean equals(Object obj) {
      Log.debug("subSPOT");
      if(obj == null) return false;
      if(obj == this) return true;
      if(!(obj instanceof VariableSymbol)) return false;
      VariableSymbol o = (VariableSymbol) obj;
      return o.type.equals(this.type) &&
        o.ident.equals(this.ident);
    }
  }

  public static class TableSymbol extends Symbol {
    public int size;

    public TableSymbol(Type type,int size, String ident) {
      this.type = type;
      this.size = size;
      this.ident = ident;
    }

    @Override public boolean equals(Object obj) {
      Log.debug("subSPOT");
      if(obj == null) return false;
      if(obj == this) return true;
      if(!(obj instanceof TableSymbol)) return false;
      TableSymbol o = (TableSymbol) obj;
      return o.type.equals(this.type) &&
        o.size == this.size &&
        o.ident.equals(this.ident);
    }
  }

  public static class FunctionSymbol extends Symbol {
    public List<VariableSymbol> arguments; // arguments is an ordered list of VariableSymbol
    public boolean defined; // false if declared but not defined

    public FunctionSymbol(Type returnType, String ident, List<VariableSymbol> arguments, boolean defined) {
      this.type = returnType;
      this.ident = ident;
      this.arguments = arguments;
      this.defined = defined;
    }

    @Override public boolean equals(Object obj) {
      if(obj == null) return false;
      if(obj == this) return true;
      if(!(obj instanceof FunctionSymbol)) return false;
      FunctionSymbol o = (FunctionSymbol) obj;
      return o.type.equals(this.type) &&
        o.ident.equals(this.ident) &&
        o.arguments.equals(this.arguments) &&
        o.defined == this.defined;
    }
  }

  // Store the table as a map
  private Map<String, Symbol> table;
  // Parent table
  private SymbolTable parent;

  // Construct a new symbol table
  public SymbolTable() {
    this.table = new HashMap<String, Symbol>();
    this.parent = null;
  }

  // Construct a new symbol table with a parent
  public SymbolTable(SymbolTable parent) {
    this.table = new HashMap<String, Symbol>();
    this.parent = parent;
  }

  // Add a new symbol
  // Returns false if the symbol cannot be added (already in the scope)
  public boolean add(Symbol sym) {
    return this.add(sym.ident, sym);
  }

  // Adds a new symbol with a given ident. The symbolic name might be altered if not free.
  // Returns false if the symbol cannot be added (already in the scope)
  // The ident is the source file's variable name, sym is its symbol in target language.
  public boolean add(String ident, Symbol sym) {
    //DEBUG
    if(!(sym.type instanceof TP2.ASD.Pointer))
      Log.debug("Adding id:"+ident+" to "+ this+", type: "+sym.type+"...");
    else
      Log.debug("Adding id:"+ident+" to "+ this+", type: pointer of "+((TP2.ASD.Pointer) sym.type).subtype+"...");
    Symbol res = this.table.get(ident);
    if(res != null) {
      Log.debug("Already present");
      return false;
    }
    Log.debug("-> "+ sym.ident);
    this.table.put(ident, getFreeSymbol(sym));
    return true;
  }


  // Remove a symbol
  // Returns false if the symbol is not in the table (without looking at parent's)
  public boolean remove(String ident) {
    return this.table.remove(ident) != null;
  }

  public Symbol lookup(String ident) {
    Log.debug("Looking to match "+ident+" in "+this+"... ");
    Symbol res = this.table.get(ident);

    if((res == null) && (this.parent != null)) {
      // Forward request
      return this.parent.lookup(ident);
    }
    if (res!=null) Log.debug("found target name " +res.ident);
    return res; // Either the symbol or null
  }

  // Returns true if the table maps a key to a symbol that have the same id as symbol.
  public boolean contains(Symbol symbol) {
    Collection<Symbol> symbols = this.table.values();
    for (Symbol table_symbol : symbols){
      if (symbol.ident == table_symbol.ident) return true;
    }
    return false;
  }

  public boolean defines(Symbol symbol) {
    Set<String> mapped_strings = this.table.keySet();
    for (String map_string : mapped_strings){
      if (map_string.equals(symbol.ident)) {return true;}
    }
    return (this.parent != null && this.parent.defines(symbol));
  }

  // Given a variable symbol, returns a similar symbol with a free id
  public Symbol getFreeSymbol(Symbol symbol) {
    Log.debug("Checking "+symbol.ident+" in "+this+ ": "+ this.defines(symbol));
    if (this.defines(symbol) ) {
      Log.debug("Getting new free symbol for "+symbol.ident);
      VariableSymbol vsymbol = (VariableSymbol)(symbol);
      return new VariableSymbol(vsymbol.type, Utils.newlab(vsymbol.ident));
    }
    return symbol;

  }

  @Override public boolean equals(Object obj) {
    if(obj == null) return false;
    if(obj == this) return true;
    if(!(obj instanceof SymbolTable)) return false;
    SymbolTable o = (SymbolTable) obj;
    return o.table.equals(this.table) &&
      ((o.parent == null && this.parent == null) || o.parent.equals(this.parent));
  }
}

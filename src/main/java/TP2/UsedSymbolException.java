package TP2;

public class UsedSymbolException extends Exception {
  public UsedSymbolException(String message) {
    super(message);
  }
}

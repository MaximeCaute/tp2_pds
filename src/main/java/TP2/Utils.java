package TP2;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.lang.String;

public class Utils {
  private static int tmp = 0;
  private static int lab = 0;
  private static int glob = 0;
  private final static Pattern re = Pattern.compile("\\\\n");

  // return "  " × level, useful for code indentation
  static public String indent(int level) {
    StringBuilder r = new StringBuilder();
    while(level-- > 0)
      r.append("  ");
    return r.toString();
  }

  // generate a new unique local identifier (starting with %)
  public static String newtmp() {
    tmp++;
    return "%tmp" + tmp;
  }

  // generate a new unique label starting with str
  public static String newlab(String str) {
    lab++;
    return scrap_end_number(str) + lab;
  }

  // generate a new unique global identifier (starting with @tmp) *)
  public static String newglob(String str) {
    glob++;
    return scrap_end_number(str) + glob;
  }

  // returns a string deprived of its end_number. For example, n1 -> n.
  public static String scrap_end_number(String str){
    String str_copy = str.substring(0, str.length());
    while ( str_copy.endsWith("1") || str_copy.endsWith("2") || str_copy.endsWith("3") ||
            str_copy.endsWith("4") || str_copy.endsWith("5") || str_copy.endsWith("6") ||
            str_copy.endsWith("7") || str_copy.endsWith("8") || str_copy.endsWith("9") ||
            str_copy.endsWith("0"))
            {str_copy = str_copy.substring(0, str_copy.length()-1);}
    return str_copy;
  }

  // transform escaped newlines ('\' 'n') into newline form suitable for LLVM
  // and append the NUL character (end of string)
  // return a pair: the new String, and its size (according to LLVM)
  public static LLVMStringConstant stringTransform(String str) {
    Matcher m = re.matcher(str);
    StringBuffer res = new StringBuffer();
    int count = 0;

    while(m.find()) {
      m.appendReplacement(res, "\\\\0A");
      count++;
    }

    m.appendTail(res).append("\\00");

    // + 1 for \00
    // - 1 by \n because each ('\' '\n') is transformed into one char
    return new LLVMStringConstant(res.toString(), 1 + str.length() - count);
  }

  // Return type of stringTransform
  public static class LLVMStringConstant {
    String str;
    int length;
    LLVMStringConstant(String str, int length) {
      this.str = str;
      this.length = length;
    }
  }

// a simple javafx.util.Pair remplacement
  public static class Pair<A,B> {
    private final A key;
    private final B value;

    public Pair(A key, B value) {
      this.key = key;
      this.value = value;
    }

    public A getKey() {
      return key;
    }

    public B getValue() {
      return value;
    }
  }

  public static class Log {
    static public boolean debug = false;
    static public void debug(String str) {
      if(debug) {
        System.out.println(str);
      }
    }
  }
}

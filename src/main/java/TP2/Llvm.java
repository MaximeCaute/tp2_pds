package TP2;

import java.util.List;
import java.util.ArrayList;
import TP2.Utils.Pair;
// This file contains a simple LLVM IR representation
// and methods to generate its string representation

public class Llvm {
  static public class IR {
    List<Instruction> header; // IR instructions to be placed before the code (global definitions)
    List<Instruction> code;   // main code

    public IR(List<Instruction> header, List<Instruction> code) {
      this.header = header;
      this.code = code;
    }

    // append an other IR
    public IR append(IR other) {
      header.addAll(other.header);
      code.addAll(other.code);
      return this;
    }

    // append a code instruction
    public IR appendCode(Instruction inst) {
      code.add(inst);
      return this;
    }

    // append a code header
    public IR appendHeader(Instruction inst) {
      header.add(inst);
      return this;
    }

    // Final string generation
    public String toString() {
      // This header describe to LLVM the target
      // and declare the external function printf
      // and scanf
      StringBuilder r = new StringBuilder("; Target\n" +
        "target triple = \"x86_64-unknown-linux-gnu\"\n" +
        "; External declaration of the printf function\n" +
        "declare i32 @printf(i8* noalias nocapture, ...)\n" +
        "declare i32 @scanf(i8* noalias nocapture, ...)\n" +
        "\n; Actual code begins\n\n");

      for(Instruction inst: header)
        r.append(inst);

      r.append("\n\n");

      for(Instruction inst: code)
        r.append(inst);

      return r.toString();
    }
  }

  // Returns a new empty list of instruction, handy
  static public List<Instruction> empty() {
    return new ArrayList<Instruction>();
  }


  // LLVM Types
  static public abstract class Type {
    public abstract String toString();
  }

  static public class Int extends Type {
    public String toString() {
      return "i32";
    }
  }

  static public class Char extends Type {
    public String toString() {
      return "i8";
    }
  }

  static public class Pointer extends Type {
    private Type subtype;

    public void setSubType(Type s) {
      this.subtype = s;
    }
    public String toString() {
      return "" + subtype.toString() + "*";
    }
  }

  static public class Bool extends Type {
    public String toString() {
      return "i1";
    }

  }

  static public class Void extends Type {
    public String toString() {
      return "void";
    }
  }

  static public class Table extends Type {
    private Type subtype;
    private int size;

    public Table(Type subtype, int size){
      this.subtype = subtype;
      this.size = size;
    }

    public String toString(){
      return "["+size+" x "+subtype.toString()+"]";
    }
  }

  // TODO : other types


  // LLVM IR Instructions
  static public abstract class Instruction {
    public abstract String toString();
  }

  static public class Add extends Instruction {
    Type type;
    String left;
    String right;
    String lvalue;

    public Add(Type type, String left, String right, String lvalue) {
      this.type = type;
      this.left = left;
      this.right = right;
      this.lvalue = lvalue;
    }

    public String toString() {
      return lvalue + " = add " + type + " " + left + ", " + right +  "\n";
    }
  }

  static public class Sub extends Instruction {
    Type type;
    String left;
    String right;
    String lvalue;

    public Sub(Type type, String left, String right, String lvalue) {
      this.type = type;
      this.left = left;
      this.right = right;
      this.lvalue = lvalue;
    }

    public String toString() {
      return lvalue + " = sub " + type + " " + left + ", " + right +  "\n";
    }
  }

  static public class Mult extends Instruction {
    Type type;
    String left;
    String right;
    String lvalue;

    public Mult(Type type, String left, String right, String lvalue) {
      this.type = type;
      this.left = left;
      this.right = right;
      this.lvalue = lvalue;
    }

    public String toString() {
      return lvalue + " = mul " + type + " " + left + ", " + right +  "\n";
    }
  }

  static public class sDiv extends Instruction {
    Type type;
    String left;
    String right;
    String lvalue;

    public sDiv(Type type, String left, String right, String lvalue) {
      this.type = type;
      this.left = left;
      this.right = right;
      this.lvalue = lvalue;
    }

    public String toString() {
      return lvalue + " = sdiv " + type + " " + left + ", " + right +  "\n";
    }
  }

  static public class Return extends Instruction {
    Type type;
    String value;

    public Return(Type type, String value) {
      this.type = type;
      this.value = value;
    }

    public String toString() {
      return "ret " + type + " " + value + "\n";
    }
  }

  static public class Declaration extends Instruction {
    String id;
    String rtype;

    public Declaration(String id, String rtype) {
      this.id = id;
      this.rtype = rtype;
    }

    public String toString() {
      return "%" + id + " = alloca " + rtype + "\n";
    }
  }

  static public class TableDeclaration extends Instruction {
    String id;
    Table table;

    public TableDeclaration(String id, Table table) {
      this.id = id;
      this.table = table;
    }

    public String toString() {
      return "%" + id + " = alloca "+table.toString()+"\n";
    }
  }

  static public class Load extends Instruction {
    String target_id;
    String source_id;
    Type target_type;
    Type source_type;

    public Load(String target_id, Type target_type, String source_id, Type source_type) {
      this.target_id = target_id;
      this.source_id = source_id;
      this.target_type = target_type;
      this.source_type = source_type;
    }

    public String toString() {
      return target_id +" = load "+target_type.toString() +", " + source_type.toString() +" "+ source_id + "\n";
    }
  }

  static public class Store extends Instruction {
    String target_id;
    String source_id;
    Type target_type;
    Type source_type;

    public Store(String source_id, Type source_type, String target_id, Type target_type) {
      this.target_id = target_id;
      this.source_id = source_id;
      this.target_type = target_type;
      this.source_type = source_type;
    }

    public String toString() {
      return "store "+source_type.toString() +" "+ source_id +", " + target_type.toString() +" "+ target_id + "\n";
    }
  }

  static public class TableLoad extends Instruction {
    Table table;
    String index_id;
    String table_id;
    String target_id;

    public TableLoad(Table source_table, String table_id, String index_id, String target_id ) {
      this.target_id = target_id;
      this.table_id = table_id;
      this.table = source_table;
      this.index_id = index_id;
    }

    public String toString() {
      return target_id +" = getelementptr " + table.toString()+", "
                                            + table.toString()+"* "
                                            + "%"+table_id +", "
                                            + "i64 0, " //?????????????????
                                            + "i32 " + index_id +"\n"
                                            ;
    }
  }

  static public class Affectation extends Instruction {
    String id;
    String rvalue;

    public Affectation(String id, String rvalue) {
      this.id = id;
      this.rvalue = rvalue;
    }

    public String toString() {
      return "%" + id + " = " + rvalue + "\n";
    }
  }
  // TODO : other instructions
  static public class CompareNotEqualZero extends Instruction {
    String var;
    Type type;
    String rvalue;

    public CompareNotEqualZero(Type type, String var, String rvalue) {
      this.var = var;
      this.type = type;
      this.rvalue = rvalue;
    }

    public String toString() {
      return rvalue + "= icmp ne " + type + " " + var + ", 0" + "\n";
    }
  }

  static public class BreakIf extends Instruction {
    String bool_id;
    Type bool_type;
    String label_then;
    String label_else;

    public BreakIf(String bool_id, Type bool_type, String label_then, String label_else) {
      this.bool_id = bool_id;
      this.bool_type = bool_type;
      this.label_then = label_then;
      this.label_else = label_else;
    }

    public String toString() {
      return "br " + bool_type + " " + bool_id + ", " + "label %" + label_then + ", " + "label %" + label_else + "\n";
    }
  }

  static public class Break extends Instruction {
    String label_break;

    public Break(String label_break) {
      this.label_break = label_break;
    }

    public String toString() {
      return "br label %" + label_break + "\n";
    }
  }

  static public class Label extends Instruction {
    String label;

    public Label(String label) {
      this.label = label;
    }

    public String toString() {
      return "" + label + ":\n";
    }
  }

  static public class Function extends Instruction {
    String id;
    List<Pair<Type, String>> variables;
    Type type;

    public Function(Type type, String id, List<Pair<Type, String>> variables) {
      this.id = id;
      this.type = type;
      this.variables = variables;
    }

    public String toString() {
      String tmp = "";
      for(Pair<Type, String> var: variables) {
        tmp = tmp + var.getKey() + " " + "%"+var.getValue() + ", ";
      }
      if(tmp != "") {
        tmp = tmp.substring(0, tmp.length() - 2);
      }
      return "define " + type + " @" + id + "(" + tmp + "){" + "\n";
    }
  }
  static public class FunctionEnd extends Instruction {

    public FunctionEnd() {
    }

    public String toString() {
      return "}\n";
    }
  }

  static public class Call extends Instruction {
    Type return_type;
    String function_name;
    List<Pair<Type, String>> variables;

    public Call(Type return_type, String function_name, List<Pair<Type, String>> variables) {
      this.return_type = return_type;
      this.function_name = function_name;
      this.variables = variables;
    }

    public String toString() {
      String tmp = "";
      for(Pair<Type, String> var: variables) {
        tmp = tmp + var.getKey() + " " + var.getValue() + ", ";
      }
      if(tmp != "") {
        tmp = tmp.substring(0, tmp.length() - 2);
      }
      return "call " + return_type + " @" + function_name + "(" + tmp + ")" + "\n";
    }
  }

  static public class CallWithReturn extends Instruction {
    Type return_type;
    String returnName;
    String function_name;
    List<Pair<Type, String>> vars;

    public CallWithReturn(Type return_type,String returnName, String function_name, List<Pair<Type, String>> vars) {
      this.return_type = return_type;
      this.returnName = returnName;
      this.function_name = function_name;
      this.vars = vars;
    }

    public String toString() {
      String tmp = "";
      for(Pair<Type, String> var: vars) {
        tmp = tmp + var.getKey() + " " + var.getValue() + ", ";
      }
      if(tmp != "") {
        tmp = tmp.substring(0, tmp.length() - 2);
      }
      return ""+returnName+" = call " + return_type + " @" + function_name + "(" + tmp + ")" + "\n";
    }
  }

  static public class Printf extends Instruction {
    int fmt_size;
    String fmt_id;
    List<Pair<Type, String>> vars;

    public Printf(int fmt_size, String fmt_id, List<Pair<Type, String>> vars) {
      this.fmt_size = fmt_size;
      this.fmt_id = fmt_id;
      this.vars = vars;
    }

    public String toString() {
      String tmp = "";
      for(Pair<Type, String> var: vars) {
        tmp = tmp + var.getKey() + " " + var.getValue() + ", ";
      }
      if(tmp != "") {
        tmp = tmp.substring(0, tmp.length() - 2);
        tmp = ", " + tmp;
      }
      return "call i32 (i8*, ...) @printf (i8* getelementptr inbounds (["+ fmt_size + " x i8], [" + fmt_size + " x i8]* @." + fmt_id + ", i64 0, i64 0)" + tmp + ")" + "\n";
    }
  }

  static public class Scanf extends Instruction {
    int fmt_size;
    String fmt_id;
    Pair<Type, String> var;

    public Scanf(int fmt_size, String fmt_id, Pair<Type, String> var) {
      this.fmt_size = fmt_size;
      this.fmt_id = fmt_id;
      this.var = var;
    }

    public String toString() {
      String tmp = "";
      tmp = tmp + var.getKey() + " " + var.getValue();
      tmp = ", " + tmp;
      return "call i32 (i8*, ...) @scanf (i8* getelementptr inbounds (["+ fmt_size + " x i8], [" + fmt_size + " x i8]* @." + fmt_id + ", i64 0, i64 0)" + tmp + ")" + "\n";
    }
  }

  static public class StaticString extends Instruction {

    String fmt_id;
    int fmt_size;
    String fmt_string;
    public StaticString(String fmt_id, int fmt_size, String fmt_string) {
      this.fmt_size = fmt_size;
      this.fmt_id = fmt_id;
      this.fmt_string = fmt_string;
    }

    public String toString() {
      return "@."+fmt_id+" = global ["+fmt_size+" x i8] c\"" + fmt_string + '"' + "\n";
    }
  }
}

package TP2.ASD;

import TP2.Llvm;

public class Table extends Type {
    public Type subtype;
    public int size;

    public Table(Type subtype, int size){
      this.subtype = subtype;
      this.size = size;
    }

    public String pp() {
      return "" + subtype.pp() + "["+size+"]";
    }

    @Override public boolean equals(Object obj) {
      return obj instanceof Table && (((Table)obj).subtype == subtype)
                                  && (((Table)obj).size == size);
    }

    public Llvm.Type toLlvmType() {
      Llvm.Pointer p =  new Llvm.Pointer();
      p.setSubType(subtype.toLlvmType());
      return p;
    }

    public String basicValue(){
      return "null";
    }
  }

package TP2.ASD;

import TP2.Llvm;

public class Pointer extends Type {
    public Type subtype;

    public Pointer(Type subtype) {
      this.subtype = subtype;
    }

    public void setSubType(Type subtype) {
      this.subtype = subtype;
    }

    public Type getSubType() {
      return this.subtype;
    }

    public String pp() {
      return "" + subtype.pp() + "*";
    }

    @Override public boolean equals(Object obj) {
      return obj instanceof Pointer && (((Pointer)obj).subtype == subtype);
    }

    public Llvm.Type toLlvmType() {
      Llvm.Pointer p =  new Llvm.Pointer();
      p.setSubType(subtype.toLlvmType());
      return p;
    }

    public String basicValue(){
      return "null";
    }
  }

package TP2.ASD;

import TP2.Llvm;
import TP2.TypeException;
import TP2.UsedSymbolException;
import TP2.SymbolTable;
import TP2.SymbolTable;

public abstract class Instruction {
    public abstract String pp();
    public abstract Instruction.SttInstruction toIR(SymbolTable st) throws TypeException;

    // Object returned by toIR on instructions, with IR + ??
    static public class SttInstruction {
      // The LLVM IR:
      public Llvm.IR ir;

      public SttInstruction(Llvm.IR ir) {
        this.ir = ir;
      }
    }
  }

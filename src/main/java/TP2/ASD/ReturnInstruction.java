package TP2.ASD;

import java.util.List;
import TP2.Llvm;
import TP2.SymbolTable;
import TP2.Utils;
import TP2.TypeException;


// Concrete class for Expression: (type) (name)
  public class ReturnInstruction extends Instruction {
    Expression returnExpression;
    public ReturnInstruction(Expression returnExpression) {
      this.returnExpression = returnExpression;
    }

    public String pp() {
      return "RETURN " + returnExpression;
    }

    public SttInstruction toIR(SymbolTable st) throws TypeException {
      // Here we simply return an empty IR
      // the `result' of this expression is the integer itself (as string)
      Expression.RetExpression ret = returnExpression.toIR(st);
      Llvm.Instruction returnInstruction = new Llvm.Return(ret.type.toLlvmType(), ret.result);
      ret.ir.appendCode(returnInstruction);
      return new SttInstruction(ret.ir);
    }
  }

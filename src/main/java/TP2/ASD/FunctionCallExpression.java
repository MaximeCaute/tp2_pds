package TP2.ASD;

import java.util.List;
import java.util.LinkedList;
import java.util.ListIterator;
import TP2.Llvm;
import TP2.SymbolTable;
import TP2.Utils;
import TP2.TypeException;
import TP2.Utils.Pair;

  public class FunctionCallExpression extends Expression {
    String id;
    List<Expression> exprs;

    public FunctionCallExpression(String id, List<Expression> exprs) {
      this.id = id;
      this.exprs = exprs;
    }

    public String pp() {
      String tmp = "";
      for(Expression expr: exprs) {
        tmp += expr.pp() + ", ";
      }
      if(tmp != "" ) {
        tmp = tmp.substring(0, tmp.length() - 2);
      }
      return "" + id + "(" + tmp + ")";
    }

    public RetExpression toIR(SymbolTable st) throws TypeException {
      // Here we simply return an empty IR
      // the `result' of this expression is the integer itself (as string)
      // SttInstruction ret = new SttInstruction(new Llvm.IR(Llvm.empty(), Llvm.empty()));

      // TODO add lookup function table as function is already declared by proto
      // and add variables contained in function proto to new st.


      // s is the function symbol if it exists
      SymbolTable.Symbol s = st.lookup(id);
      if(s instanceof SymbolTable.FunctionSymbol) {
        SymbolTable.FunctionSymbol fn = SymbolTable.FunctionSymbol.class.cast(s);

        Type type = fn.type;
        String result = Utils.newtmp();
        RetExpression ret = new RetExpression(
            new Llvm.IR(Llvm.empty(), Llvm.empty()), type, result);
        // TODO check types
        LinkedList<Pair<Llvm.Type, String>> parameters = new LinkedList<>();
        Expression.RetExpression ret_expr;
        for(Expression expr: exprs) {
          ret_expr = expr.toIR(st);
          ret.ir.append(ret_expr.ir);
          parameters.add(new Pair(new Llvm.Int(), ret_expr.result));
        }
        ret.ir.appendCode(new Llvm.CallWithReturn(fn.type.toLlvmType(), result, fn.ident, parameters));

        return ret;
      }
      throw new TypeException("Function is not declared...");
    }
  }

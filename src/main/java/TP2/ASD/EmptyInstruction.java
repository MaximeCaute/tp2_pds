package TP2.ASD;

import TP2.Llvm;
import TP2.TypeException;
import TP2.UsedSymbolException;
import TP2.SymbolTable;
import TP2.SymbolTable;

public class EmptyInstruction extends Instruction {
    public String pp(){
      return "";
    }
    public SttInstruction toIR(SymbolTable st) throws TypeException{
      return new SttInstruction(new Llvm.IR(Llvm.empty(), Llvm.empty()));
    }
  }

package TP2.ASD;

import TP2.Llvm;
import TP2.TypeException;
import TP2.Utils;
import TP2.SymbolTable;

// Concrete class for Instruction: (name) := expr
  public class AffectationInstruction extends Instruction {
    String id;
    Expression right_expr;
    public AffectationInstruction(String id, Expression right) {
      this.id = id;
      this.right_expr = right;
    }

    public String pp() {
      return "" + id + " := " + right_expr.pp();
    }


    public SttInstruction toIR(SymbolTable st) throws TypeException {
      Expression.RetExpression rightRet = right_expr.toIR(st);

      SymbolTable.Symbol s = st.lookup(id);
      if(s == null) {
        throw new TypeException("Cannot find identifier: " + id);
      }
      if(s instanceof SymbolTable.VariableSymbol) {
        SymbolTable.VariableSymbol sv = SymbolTable.VariableSymbol.class.cast(s);


        String result = "%"+ sv.ident;

        if (! (sv.type instanceof Pointer) )
          throw new TypeException("Not a pointer: "+ sv.type.pp());
        Pointer pointer_type = (Pointer) sv.type;
        Type value_type = pointer_type.subtype;

        //TODO check Types (use symbol table)
        Llvm.Instruction store = new Llvm.Store(rightRet.result, value_type.toLlvmType(), result, pointer_type.toLlvmType());
        //Llvm.Instruction aff = new Llvm.Affectation(result, rightRet.result);
        rightRet.ir.appendCode(store);

        return new SttInstruction(rightRet.ir);
      }

      throw new TypeException("Unsupported type of Symbol");
    }
  }

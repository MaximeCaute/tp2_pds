package TP2.ASD;

import java.util.List;
import java.util.LinkedList;
import java.util.ListIterator;
import TP2.Llvm;
import TP2.SymbolTable;
import TP2.Utils;
import TP2.TypeException;
import TP2.Utils.Pair;
import java.util.Collections;
import TP2.Utils.Log;


  public class PrintInstruction extends Instruction {
    List<Object> parameters;
    public PrintInstruction(List<Object> parameters) {
      this.parameters = parameters;
      Collections.reverse(this.parameters);
    }

    public String pp() {
      String tmp = "";
      for(Object param: parameters) {
        if(param instanceof String) {
          tmp += param + ", ";
        } else {
          tmp += ((Expression) param).pp() + ", ";
        }
      }
      if(tmp != "") {
        tmp = tmp.substring(0, tmp.length() - 2);
      }
      return "PRINT " + tmp;
    }

    public SttInstruction toIR(SymbolTable st) throws TypeException {

      // No type inference we assume int type for variables
        SttInstruction ret = new SttInstruction(new Llvm.IR(Llvm.empty(), Llvm.empty()));
        String format_string = "";
        Expression tmp_expr;
        Expression.RetExpression tmp_ret_expr;
        LinkedList<Pair<Llvm.Type, String>> final_parameters = new LinkedList<>();

        // Create parameter list and format string for printf
        // at the same time by iterating args list
        for(Object param: parameters) {
          if(param instanceof Expression) {
            tmp_expr = (Expression) param;
            tmp_ret_expr = tmp_expr.toIR(st);
            ret.ir.append(tmp_ret_expr.ir);
            format_string += "%d";
            final_parameters.add(new Pair(tmp_ret_expr.type.toLlvmType(), tmp_ret_expr.result));
          }
          else if(param instanceof String) {
            format_string += (String)param;
          }
          else {
            throw new TypeException("Type Error in PRINT parameters");
          }
        }
        // format the string
        Log.debug("format string: "+format_string);
        String final_formated_string = format_string.replace("\\n", "\\0A");
        final_formated_string += "\\00";
        Log.debug("final formated string: "+final_formated_string);
        int fmt_length  = format_string.length() + 1 - (format_string.length() - format_string.replace("\\n", "n").length());
        // get new id
        String fmt_id = Utils.newlab("fmt");
        // add static string to header
        ret.ir.appendHeader(new Llvm.StaticString(fmt_id, fmt_length, final_formated_string));
        // add print instruction
        ret.ir.appendCode(new Llvm.Printf(fmt_length, fmt_id, final_parameters));
        return ret;
    }
  }

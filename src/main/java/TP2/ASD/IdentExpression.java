package TP2.ASD;

import TP2.Llvm;
import TP2.TypeException;
import TP2.SymbolTable;
import TP2.Utils;
import TP2.Utils.Log;

// Concrete class for Expression: identifier case
  public class IdentExpression extends Expression {
    String ident;
    public IdentExpression(String ident) {
      this.ident = ident;
    }

    public String pp() {
      return "" + ident;
    }

    public RetExpression toIR(SymbolTable st) throws TypeException {
      // Here we simply return an empty IR
      // the `result' of this expression is the integer itself (as string)
      // TODO
      Log.debug("DEBUG: "+ this.pp());
      SymbolTable.Symbol s = st.lookup(ident);
      if(s == null) {
        throw new TypeException("Cannot find identifier: " + ident);
      }
      if(s instanceof SymbolTable.VariableSymbol) {
        SymbolTable.VariableSymbol sv = SymbolTable.VariableSymbol.class.cast(s);

        String result = Utils.newtmp();
        String source_pointer_id = "%"+sv.ident;

        //TODO
        if (! (sv.type instanceof Pointer) )
          throw new TypeException("Not a pointer: " + sv.type.pp());
        Pointer pointer_type = (Pointer) sv.type;
        Type value_type = pointer_type.subtype;

        Llvm.Instruction load = new Llvm.Load(result, value_type.toLlvmType(), source_pointer_id, pointer_type.toLlvmType());
        Llvm.IR ir = new Llvm.IR(Llvm.empty(), Llvm.empty());

        ir.appendCode(load);
        return new RetExpression(ir, value_type, result);

        //return new RetExpression(new Llvm.IR(Llvm.empty(), Llvm.empty()), sv.type, "%" + sv.ident);
      }
      throw new TypeException("Unsupported type of Symbol");
    }
  }

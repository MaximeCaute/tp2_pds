package TP2.ASD;

import TP2.Llvm;
import TP2.TypeException;
import TP2.SymbolTable;
import java.util.List;
import java.util.LinkedList;


public class Program {
    Instruction i; // What a program contains. TODO : change when you extend the language
    List<Instruction> instructions;

    public Program(Instruction i) {
      this.i = i;
    }

    public Program(List<Instruction> instructions) {
      this.instructions = instructions;
    }

    // Pretty-printer
    public String pp() {
      String prettyPrint = "";
      for (Instruction i: instructions) prettyPrint+= i.pp();
      return prettyPrint;
    }

    // IR generation
    public Llvm.IR toIR() throws TypeException {
      // TODO : change when you extend the language
      SymbolTable st = new SymbolTable();
      st.add(new SymbolTable.FunctionSymbol(new TP2.ASD.Void(), "main", new LinkedList(), false));
      // computes the IR of the expression
      //Instruction.SttInstruction sttInstr = i.toIR(st);
      Instruction.SttInstruction sttInstr = new Instruction.SttInstruction(new Llvm.IR(Llvm.empty(), Llvm.empty()));
      for(Instruction i: instructions){
        sttInstr.ir.append(i.toIR(st).ir);
      }
      // add a return instruction
      //Llvm.Instruction ret = new Llvm.Return(new Llvm.Int(), "0");
      //sttInstr.ir.appendCode(ret);

      return sttInstr.ir;
    }
  }

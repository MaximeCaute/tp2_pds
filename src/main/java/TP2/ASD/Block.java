package TP2.ASD;

import java.util.List;
import java.util.Collections;
import TP2.Llvm;
import TP2.TypeException;
import TP2.UsedSymbolException;
import TP2.SymbolTable;

public class Block extends Instruction {
    List<DeclarationInstruction> decl_list; // What is declared in a bloc
    List<Instruction> instr_list; // What is contained in a bloc

    public Block(List<DeclarationInstruction> decl_list, List<Instruction> instr_list) {
      this.decl_list = decl_list;
      this.instr_list = instr_list;
      // instructions are empiled (so in reverse)
      Collections.reverse(this.decl_list);
      Collections.reverse(this.instr_list);
    }


    // Pretty-printer
    public String pp() {
      String s = "{\n";
      for(DeclarationInstruction decl: decl_list)
        s = s + decl.pp() + "\n";
      s = s + "\n";
      for(Instruction instr: instr_list)
        s = s + instr.pp() + "\n";
      s = s + "}";
      return s;
    }

    // IR generation
    public SttInstruction toIR(SymbolTable st) throws TypeException {
      // do effectation
      SymbolTable st_new = new SymbolTable(st);
      /*
      for(DeclarationExpression decl: decl_list) {
          if(!st_new.add(new SymbolTable.VariableSymbol(decl.type, decl.id))) {
            throw new TypeException("Symbol: '" + decl.id + "' is already declared");
          }
      }
      */
      // computes the IR of the expression
      Instruction.SttInstruction stt = new SttInstruction(new Llvm.IR(Llvm.empty(), Llvm.empty()));
      // add a return instruction

      for(DeclarationInstruction decl: decl_list)
        stt.ir.append(decl.toIR(st_new).ir);
      for(Instruction instr: instr_list)
        stt.ir.append(instr.toIR(st_new).ir);

      return stt;
    }
  }

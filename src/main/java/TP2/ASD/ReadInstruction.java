package TP2.ASD;

import java.util.List;
import java.util.LinkedList;
import java.util.ListIterator;
import TP2.Llvm;
import TP2.SymbolTable;
import TP2.Utils;
import TP2.TypeException;
import TP2.Utils.Pair;
import TP2.Utils.Log;

public class ReadInstruction extends Instruction {
  List<Object> params;
  public ReadInstruction(List<Object> params) {
    this.params = params;
  }

  public String pp() {
    String tmp = "";
    for(Object param: params) {
      if(param instanceof String) {
        tmp += param + ", ";
      } else if (param instanceof Pair){
        tmp += ((Pair<String, Expression>) param).getKey() + "["
        + ((Pair<String, Expression>) param).getValue().pp() + "]"
        + ", ";
      }
      else { tmp += "ERROR, "; }
    }
    if(tmp != "") {
      tmp = tmp.substring(0, tmp.length() - 2);
    }
    return "READ " + tmp;
  }

  public SttInstruction toIR(SymbolTable st) throws TypeException {

    SttInstruction ret = new SttInstruction(new Llvm.IR(Llvm.empty(), Llvm.empty()));
    // We iter on read parameters
    for(Object param: params) {

      // Object is a variable -> read it with scanf
      if(param instanceof String) {
        SymbolTable.Symbol s = st.lookup((String)param);
        if(s instanceof SymbolTable.VariableSymbol) {
          // Now we have a variable
          SymbolTable.VariableSymbol sv = (SymbolTable.VariableSymbol) s;
          // Type check it
          if(!(sv.type instanceof Pointer)) {
            throw new TypeException("Cannot READ to non pointer variable");
          }
          // create format string
          String final_formated_string = "%d";
          final_formated_string += "\\00";
          int fmt_length = 3;
          // get new id
          String fmt_id = Utils.newlab("fmt");
          // add static string to header
          ret.ir.appendHeader(new Llvm.StaticString(fmt_id, fmt_length, final_formated_string));
          // add print instruction
          ret.ir.appendCode(new Llvm.Scanf(fmt_length, fmt_id, new Pair(sv.type.toLlvmType(), "%"+sv.ident)));
        }
        else {
          throw new TypeException("You are not reading a defined variable: " + param);
        }
      }
      // table object -> instanciate a variable and read it with Scanf then store it back into the table
      else if (param instanceof Pair){
        Pair<String, Expression> param_table = (Pair<String, Expression>)param;
        SymbolTable.Symbol s = st.lookup(param_table.getKey());
        if(s instanceof SymbolTable.TableSymbol) {
          // declare a temporary value holder
          String var_id = Utils.newlab("i");
          ret.ir.appendCode(new Llvm.Declaration(var_id, (new Llvm.Int()).toString()));
          // and add it to symbol table
          SymbolTable.Symbol sym = new SymbolTable.VariableSymbol(new Pointer(new Int()), var_id);
          st.add(sym);
          // call read onto the variable
          LinkedList<Object> sub_var = new LinkedList<Object>();
          sub_var.add(var_id);
          ret.ir.append(new ReadInstruction(sub_var).toIR(st).ir);
          // store it back
          ret.ir.append((new TableAffectationInstruction(s.ident,param_table.getValue(), new IdentExpression(var_id))).toIR(st).ir);
        }
        else {
          throw new TypeException("You are not reading a defined table");
        }
      }
      else {
        throw new TypeException("Unsupported read type");
      }
    }
    return ret;
  }
}

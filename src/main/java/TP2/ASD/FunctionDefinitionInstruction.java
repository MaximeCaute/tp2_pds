package TP2.ASD;

import java.util.List;
import java.util.LinkedList;
import java.util.ListIterator;
import TP2.Llvm;
import TP2.SymbolTable;
import TP2.Utils;
import TP2.TypeException;
import TP2.Utils.Log;
import TP2.Utils.Pair;

  public class FunctionDefinitionInstruction extends Instruction {
    String id;
    Type type;
    Instruction body;
    List<String> variables;

    public FunctionDefinitionInstruction(Type type, String id, List<String> variables, Instruction body) {
      this.id = id;
      this.type = type;
      this.body = body;
      this.variables = variables;
    }

    public String pp() {
      String tmp = "";
      for(String var_id: variables) {
        tmp += var_id + ", ";
      }
      if(tmp != "") {
        tmp = tmp.substring(0, tmp.length() - 2);
      }
      return "FUNC " + type.pp() + " " + id + " (" + tmp + ")\n{" + body.pp() + "\n}";
    }

    public SttInstruction toIR(SymbolTable st) throws TypeException {
      // Here we simply return an empty IR
      // the `result' of this expression is the integer itself (as string)
      // SttInstruction ret = new SttInstruction(new Llvm.IR(Llvm.empty(), Llvm.empty()));

      // TODO add lookup function table as function is already declared by proto
      // and add variables contained in function proto to new st.

      // No type inference we assume int type for variables
      SymbolTable.Symbol s= st.lookup(id);
      // This case is for a declaration & definition instruction (no PROTO)
      if(s == null) {
        LinkedList<SymbolTable.VariableSymbol> variables_symbols = new LinkedList();
        for(String var: variables) {
          variables_symbols.add(new SymbolTable.VariableSymbol(new Int(), var));
        }
        st.add(new SymbolTable.FunctionSymbol(type, id, variables_symbols, true));
      }
      // Now s is the function symbol we want
      s = st.lookup(id);
      if(s instanceof SymbolTable.FunctionSymbol) {
        SymbolTable.FunctionSymbol fn = SymbolTable.FunctionSymbol.class.cast(s);
        // TODO finish type checking and function lookup
        SymbolTable st_func = new SymbolTable(st);
        LinkedList<Pair<Llvm.Type, String>> typedVariables = new LinkedList<Pair<Llvm.Type, String>>();
        for(String var: variables) {
          typedVariables.add(new Pair(new Int().toLlvmType(), var));
        }

        SttInstruction ret = new SttInstruction(new Llvm.IR(Llvm.empty(), Llvm.empty()));
        ret.ir.appendCode(new Llvm.Function(type.toLlvmType(), id, typedVariables));

        LinkedList<String> local_variables = new LinkedList<String>();
        for(String variable: variables) {
          String local_variable_id = Utils.newlab(variable);
          local_variables.add(local_variable_id);
          //Log.debug("Symbol table: "+st_func+" adds mapping "+variable+" -> "+local_variable_id);
          //st_func.add(variable, new SymbolTable.VariableSymbol(new Pointer(new Int()), local_variable_id));
        }

        //REPLACED WITH BELOW
        //ret.ir.append(new DeclarationInstruction(new Int(), local_variables).toIR(st_func).ir);
        ListIterator<String> local_iterator = local_variables.listIterator();
        ListIterator<String> global_iterator = variables.listIterator();
        String local_variable;
        String global_variable;
        Type varType = new Int();
        while(local_iterator.hasNext()) {
          local_variable = local_iterator.next();
          global_variable = global_iterator.next();

          SymbolTable.VariableSymbol local_symbol =
            new SymbolTable.VariableSymbol(new Pointer(varType), local_variable);

          while(st_func.defines(local_symbol)){
            String new_id =  Utils.newlab(local_symbol.ident);
            Log.debug("Changing lab: "+local_symbol.ident+" -> "+ new_id);
            local_symbol = new SymbolTable.VariableSymbol(new Pointer(varType), new_id);
          }

          Log.debug("FUNC_DEF:Symbol table: "+st+" adds mapping "+global_variable+" -> "+local_symbol.ident);
          boolean added = st.add(global_variable, local_symbol);

          if(!added) {
            throw new TypeException("Symbol: '" + global_variable + "' is already declared");
          }
          Llvm.Instruction aff =  new Llvm.Declaration(local_symbol.ident, varType.toLlvmType().toString());
          ret.ir.appendCode(aff);
        }
        //END REPLACE

        //ListIterator<String> local_iterator = local_variables.listIterator();
        //ListIterator<String> global_iterator = variables.listIterator();
        //String local_variable;
        //String global_variable;
        String local_variable_name;
        /*while(local_iterator.hasNext()){
          local_variable = local_iterator.next();
          global_variable = global_iterator.next();

          //TODO Maybe Change
          Llvm.Pointer local_pointer = new Llvm.Pointer();
          local_pointer.setSubType(new Llvm.Int());
          //TODO Stop assuming everything is Int-typed
          ret.ir.appendCode(new Llvm.Store("%"+ global_variable, new Llvm.Int(), "%"+local_variable, local_pointer ));
        }*/

        for (String global_variable_name: variables) {
          Llvm.Pointer local_pointer = new Llvm.Pointer();
          local_pointer.setSubType(new Llvm.Int());


          Log.debug("FUNC_DECL: Symbol table: "+st_func+" tries to map "+ global_variable_name+": results "+ st_func.lookup(global_variable_name));
          local_variable_name = st_func.lookup(global_variable_name).ident;

          Log.debug("FUNC_DECL: Symbol table: "+st_func+" maps "+ global_variable_name +" -> "+local_variable_name);
          ret.ir.appendCode(new Llvm.Store("%"+ global_variable_name, new Llvm.Int(), "%"+local_variable_name, local_pointer ));

        }
        ret.ir.append(body.toIR(st_func).ir);
        ret.ir.appendCode(new Llvm.Return(type.toLlvmType(), type.basicValue()));
        ret.ir.appendCode(new Llvm.FunctionEnd());
        return ret;
      }
      throw new TypeException("Unsupported type of Symbol");
    }
  }

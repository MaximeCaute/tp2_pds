package TP2.ASD;

import java.util.List;
import TP2.Llvm;
import TP2.SymbolTable;
import TP2.SymbolTable.FunctionSymbol;
import TP2.Utils;
import TP2.TypeException;
import java.util.LinkedList;


  public class FunctionDeclarationInstruction extends Instruction {
    String id;
    Type type;
    Instruction body;
    List<String> variables;

    public FunctionDeclarationInstruction(Type type, String id, List<String> variables) {
      this.id = id;
      this.type = type;
      this.variables = variables;
    }

    public String pp() {
      String tmp = "";
      for(String var_id: variables) {
        tmp += var_id + ", ";
      }
      if(tmp != "" ) {
        tmp = tmp.substring(0, tmp.length() - 2);
      }
      return "PROTO FUNC " + type.pp() + " " + id + " (" + tmp + ")\n";
    }

    public SttInstruction toIR(SymbolTable st) throws TypeException {
      // Here we simply return an empty IR
      // the `result' of this expression is the integer itself (as string)
      // SttInstruction ret = new SttInstruction(new Llvm.IR(Llvm.empty(), Llvm.empty()));

      // We assume that all variables are Int because type inference is complicated.
      LinkedList<SymbolTable.VariableSymbol> var_sym = new LinkedList();
      for(String var: variables) {
        var_sym.add(new SymbolTable.VariableSymbol(new Int(), var));
      }
      st.add(new SymbolTable.FunctionSymbol(type, id, var_sym, false));
      SttInstruction ret = new SttInstruction(new Llvm.IR(Llvm.empty(), Llvm.empty()));
      return ret;
    }
  }

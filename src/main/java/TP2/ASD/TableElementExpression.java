package TP2.ASD;

import TP2.Llvm;
import TP2.TypeException;
import TP2.SymbolTable;
import TP2.Utils;
import TP2.Utils.Log;

// Concrete class for Expression: identifier case
  public class TableElementExpression extends Expression {
    String ident;
    Expression index;
    public TableElementExpression(String ident, Expression index) {
        this.ident = ident;
        this.index = index;
    }

    public String pp() {
      return "" + ident+"["+index+"]";
    }

    public RetExpression toIR(SymbolTable st) throws TypeException {
      // Here we simply return an empty IR
      // the `result' of this expression is the integer itself (as string)
      // TODO
      RetExpression ret = index.toIR(st);

      Log.debug("DEBUG: "+ this.pp());
      SymbolTable.Symbol s = st.lookup(ident);
      if(s == null) {
        throw new TypeException("Cannot find identifier: " + ident);
      }
      if(s instanceof SymbolTable.TableSymbol) {
        SymbolTable.TableSymbol sv = SymbolTable.TableSymbol.class.cast(s);

        String tableResult = Utils.newtmp();
        String source_pointer_id = "%"+sv.ident;

        //TODO
        if (! (sv.type instanceof Pointer) )
          throw new TypeException("Not a pointer: " + sv.type.pp());
        Pointer pointer_type = (Pointer) sv.type;
        Type value_type = pointer_type.subtype;

        Llvm.Table table = new Llvm.Table(value_type.toLlvmType(), sv.size);
        Llvm.Instruction pointerLoad = new Llvm.TableLoad( table,
                                                    ident,
                                                    ret.result,
                                                    tableResult);
        ret.ir.appendCode(pointerLoad);

        String result = Utils.newtmp();
        Llvm.Instruction load = new Llvm.Load(      result,
                                                    value_type.toLlvmType(),
                                                    tableResult,
                                                    new Pointer(value_type).toLlvmType());
        ret.ir.appendCode(load);

        return new RetExpression(ret.ir, value_type, result);

        //return new RetExpression(new Llvm.IR(Llvm.empty(), Llvm.empty()), sv.type, "%" + sv.ident);
      }
      throw new TypeException("Unsupported type of Symbol");
    }
  }

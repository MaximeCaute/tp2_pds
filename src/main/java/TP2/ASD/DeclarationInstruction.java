package TP2.ASD;

import java.util.List;
import TP2.Llvm;
import TP2.SymbolTable;
import TP2.Utils;
import TP2.TypeException;
import TP2.Utils.Log;

// Concrete class for Expression: (type) (name)
  public class DeclarationInstruction extends Instruction {
    //var ids, actually
    List<String> ids;
    List<Utils.Pair<Integer, String>> tables_size_and_ids_list;
    Type type;
    public DeclarationInstruction(Type type, List<String> var_ids_list,
                                  List<Utils.Pair<Integer, String>> tables_size_and_ids_list) {
      this.ids = var_ids_list;
      this.tables_size_and_ids_list = tables_size_and_ids_list;
      this.type = type;
    }

    public String pp() {
      String tmp = "";
      for(String id: ids) {
        tmp += id + ", ";
      }
      return "" + type.pp() + " " + tmp;
    }

    public SttInstruction toIR(SymbolTable st) throws TypeException {
      // Here we simply return an empty IR
      // the `result' of this expression is the integer itself (as string)
      Log.debug(this.pp());
      SttInstruction ret = new SttInstruction(new Llvm.IR(Llvm.empty(), Llvm.empty()));

      // Variables declaration
      for(String id: ids) {
        SymbolTable.Symbol sym = new SymbolTable.VariableSymbol(new Pointer(this.type), id);

        //DEBUG
        int c = 0;

        while(st.defines(sym)){
          String new_id =  Utils.newlab(id);
          Log.debug("DECL: Changing label: "+id+" -> "+ new_id);
          if (c==20) throw new TypeException("DEBUG");
          sym = new SymbolTable.VariableSymbol(new Pointer(this.type), new_id);
          c++;
        }
        Log.debug("DECL : Symbol table: "+st+" adds mapping "+id+" -> "+sym.ident);
        boolean added = st.add(id, sym);

        if(!added) {
          throw new TypeException("Symbol: '" + id + "' is already declared");
        }
        Llvm.Instruction aff =  new Llvm.Declaration(sym.ident, type.toLlvmType().toString());
        ret.ir.appendCode(aff);
      }

      // Tables declaration
      for(Utils.Pair<Integer,String> size_and_id: tables_size_and_ids_list){
        String id = size_and_id.getValue();
        int size = size_and_id.getKey();
        SymbolTable.Symbol sym = new SymbolTable.TableSymbol(new Pointer(this.type), size, id);

        while(st.defines(sym)){
          String new_id =  Utils.newlab(id);
          Log.debug("DECL: Changing table label: "+id+" -> "+ new_id);
          sym = new SymbolTable.TableSymbol(new Pointer(this.type), size , new_id);
        }

        boolean added = st.add(id, sym);
        if(!added) {
          throw new TypeException("Symbol: '" + id + "' is already declared");
        }
        Llvm.Table table = new Llvm.Table(type.toLlvmType(), size);
        Llvm.Instruction aff =  new Llvm.TableDeclaration(sym.ident, table);
        ret.ir.appendCode(aff);
      }
      return ret;
    }
  }

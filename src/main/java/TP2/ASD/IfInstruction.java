package TP2.ASD;

import TP2.Llvm;
import TP2.TypeException;
import TP2.Utils;
import TP2.SymbolTable;

// Special expression If Class
  public class IfInstruction extends Instruction {
    Expression expr_cond;
    Instruction instr_then;
    Instruction instr_else;

    public IfInstruction(Expression expr_cond, Instruction instr_then, Instruction instr_else) {
      this.expr_cond = expr_cond;
      this.instr_then = instr_then;
      this.instr_else = instr_else;
    }

    // Pretty-printer
    public String pp() {
      return "IF" + expr_cond.pp() + "\nTHEN\n" + instr_then.pp() + "\nELSE\n" + instr_else.pp() + "\nFI";
    }

    // IR generation
    public SttInstruction toIR(SymbolTable st) throws TypeException {
      Expression.RetExpression condRet = expr_cond.toIR(st);

      SttInstruction thenStt = instr_then.toIR(st);
      SttInstruction elseStt = instr_else.toIR(st);
/*
      // We check if the types mismatches
      if(!leftRet.type.equals(rightRet.type)) {
        throw new TypeException("type mismatch: have " + leftRet.type + " and " + rightRet.type);
      }
*/
      // allocate a new identifier for the result
      String result_comp = Utils.newtmp();
      String then_label = Utils.newlab("then");
      String else_label = Utils.newlab("else");
      String fi_label = Utils.newlab("fi");

      // new add instructions needed for the if
      // We base our build on the condIR
      Llvm.Instruction instr_then_label = new Llvm.Label(then_label);
      Llvm.Instruction instr_else_label = new Llvm.Label(else_label);
      Llvm.Instruction instr_fi_label = new Llvm.Label(fi_label);
      Llvm.Instruction comp = new Llvm.CompareNotEqualZero(condRet.type.toLlvmType(), condRet.result, result_comp);
      Llvm.Instruction break_if = new Llvm.BreakIf(result_comp,  new Llvm.Bool(), then_label, else_label);
      Llvm.Instruction break_to_fi = new Llvm.Break(fi_label);

      // append theses instructions to create a if block
      condRet.ir.appendCode(comp);
      condRet.ir.appendCode(break_if);
      condRet.ir.appendCode(instr_then_label);
      condRet.ir.append(thenStt.ir);
      condRet.ir.appendCode(break_to_fi);
      condRet.ir.appendCode(instr_else_label);
      condRet.ir.append(elseStt.ir);
      condRet.ir.appendCode(break_to_fi);
      condRet.ir.appendCode(instr_fi_label);
      // return the generated IR, plus the type of this expression
      // and where to find its result
      //String result_if = Utils.newtmp();
      return new SttInstruction(condRet.ir);
    }
  }

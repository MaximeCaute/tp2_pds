package TP2.ASD;

import TP2.Llvm;
import TP2.TypeException;
import TP2.Utils;
import TP2.SymbolTable;

// Special expression While Class
  public class WhileInstruction extends Instruction {
    Expression expr_cond;
    Instruction instr_block;

    public WhileInstruction(Expression expr_cond, Instruction instr_block) {
      this.expr_cond = expr_cond;
      this.instr_block = instr_block;
    }

    // Pretty-printer
    public String pp() {
      return "WHILE" + expr_cond.pp() + "\nDO\n{\n" + instr_block.pp() + "\n}\nDONE";
    }

    // IR generation
    public SttInstruction toIR(SymbolTable st) throws TypeException {
      SttInstruction whileStt = new SttInstruction(new Llvm.IR(Llvm.empty(), Llvm.empty()));
      Expression.RetExpression condRet = expr_cond.toIR(st);

      SttInstruction blockStt = instr_block.toIR(st);
/*
      // We check if the types mismatches
      if(!leftRet.type.equals(rightRet.type)) {
        throw new TypeException("type mismatch: have " + leftRet.type + " and " + rightRet.type);
      }
*/
      // allocate a new identifier for the result
      String result_comp = Utils.newtmp();
      String while_label = Utils.newlab("while");
      String do_label = Utils.newlab("do");
      String done_label = Utils.newlab("done");

      // new add instructions needed for the while
      // We base our build on the condIR
      Llvm.Instruction instr_while_label = new Llvm.Label(while_label);
      Llvm.Instruction instr_do_label = new Llvm.Label(do_label);
      Llvm.Instruction instr_done_label = new Llvm.Label(done_label);
      Llvm.Instruction comp = new Llvm.CompareNotEqualZero(condRet.type.toLlvmType(), condRet.result, result_comp);
      Llvm.Instruction break_if = new Llvm.BreakIf(result_comp,  new Llvm.Bool(), do_label, done_label);
      Llvm.Instruction break_to_while = new Llvm.Break(while_label);

      // append theses instructions to create a while block
      whileStt.ir.appendCode(break_to_while);
      whileStt.ir.appendCode(instr_while_label);
      whileStt.ir.append(condRet.ir);
      whileStt.ir.appendCode(comp);
      whileStt.ir.appendCode(break_if);

      whileStt.ir.appendCode(instr_do_label);
      whileStt.ir.append(blockStt.ir);
      whileStt.ir.appendCode(break_to_while);

      whileStt.ir.appendCode(instr_done_label);
      // return the generated IR, plus the type of this expression
      // and where to find its result
      return new SttInstruction(whileStt.ir);
    }
  }

package TP2.ASD;

import TP2.Llvm;
import TP2.TypeException;
import TP2.Utils;
import TP2.SymbolTable;

// Concrete class for Instruction: (name) := expr
  public class TableAffectationInstruction extends Instruction {
    String id;
    Expression index;
    Expression rightExpr;

    public TableAffectationInstruction(String id,Expression index, Expression right) {
      this.id = id;
      this.index = index;
      this.rightExpr = right;
    }

    public String pp() {
      return "" + id +"["+index.pp()+"] := " + rightExpr.pp();
    }


    public SttInstruction toIR(SymbolTable st) throws TypeException {
      Expression.RetExpression indexRet = index.toIR(st);
      Expression.RetExpression rightRet = rightExpr.toIR(st);
      rightRet.ir.append(indexRet.ir);

      SymbolTable.Symbol s = st.lookup(id);
      if(s == null) {
        throw new TypeException("Cannot find identifier: " + id);
      }
      if(s instanceof SymbolTable.TableSymbol) {
        SymbolTable.TableSymbol sv = SymbolTable.TableSymbol.class.cast(s);


        String result = "%"+ Utils.newlab(sv.ident);

        if (! (sv.type instanceof Pointer) )
          throw new TypeException("Not a pointer: "+ sv.type.pp());
        Pointer pointer_type = (Pointer) sv.type;
        Type value_type = pointer_type.subtype;

        //TODO check Types (use symbol table)
        Llvm.Table table = new Llvm.Table(value_type.toLlvmType(), sv.size);
        Llvm.Instruction load = new Llvm.TableLoad(   table,
                                                      id,
                                                      indexRet.result,
                                                      result);
        //Llvm.Instruction aff = new Llvm.Affectation(result, rightRet.result);
        rightRet.ir.appendCode(load);
        Llvm.Instruction store = new Llvm.Store(rightRet.result, value_type.toLlvmType(), result, pointer_type.toLlvmType() );
        rightRet.ir.appendCode(store);

        return new SttInstruction(rightRet.ir);
      }

      throw new TypeException("Unsupported type of Symbol");
    }
  }

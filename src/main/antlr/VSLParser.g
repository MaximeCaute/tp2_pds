parser grammar VSLParser;

options {
  language = Java;
  tokenVocab = VSLLexer;
}

@header {
  package TP2;

  import java.util.stream.Collectors;
  import java.util.Arrays;
  import java.util.LinkedList;
  import TP2.Utils.Pair;
}

@members {
}


// TODO : other rules

program returns [TP2.ASD.Program out]
    : //main_func = main_function
      prg = program_block
      EOF { $out = new TP2.ASD.Program($prg.out); } // TODO : change when you extend the language
    ;

program_block returns [LinkedList<TP2.ASD.Instruction> out]
    : prgm_brick=program_brick
      {
        LinkedList<TP2.ASD.Instruction> blockList = new LinkedList<TP2.ASD.Instruction>();
        blockList.add($prgm_brick.out);
        $out=blockList;
      }
    | prgm_brick=program_brick prgm_blck=program_block
      {
        LinkedList<TP2.ASD.Instruction> blockList = $prgm_blck.out;
        blockList.add($prgm_brick.out);
        $out=blockList;
      }
    ;

// A program's brick is either a function or variable declaration, or a function definition
program_brick returns [TP2.ASD.Instruction out]
    : decl=declaration {$out=$decl.out;}
    | func_decl= function_declaration {$out=$func_decl.out;}
    | main=main_function_definition {$out=$main.out;}
    | func_def=function_definition {$out=$func_def.out;}
    ;

main_function_definition returns [TP2.ASD.Instruction out]
    : FUNC ret_type=func_type MAIN LP RP instr=instruction {
        LinkedList<String> args = new LinkedList<String>();
        $out = new TP2.ASD.FunctionDefinitionInstruction($ret_type.out, "main", args, $instr.out);
      }
    ;

function_definition returns [TP2.ASD.Instruction out]
    : FUNC ret_type=func_type id=IDENT
        LP args=function_arguments_list RP
        instr=instruction {
          $out = new TP2.ASD.FunctionDefinitionInstruction($ret_type.out, $id.text, $args.out, $instr.out);
      }
    ;


function_arguments_list returns [List<String> out]
    : id=identifier COMMA next_ids=identifiers_list {
      //TODO CORRECT THIS! ONLY VARIABLES CONSIDERED!
      List<String> idl = $next_ids.out.getKey();
      idl.add($id.out);
      $out=idl;
      }
    | id=identifier {
      List<String> idl = new LinkedList<String>();
      idl.add($id.out);
      $out=idl;
      }
    | {$out = new LinkedList<String>();}
    ;

block returns [TP2.ASD.Instruction out]
    : decll=declaration_list instrl=instruction_list { $out = new TP2.ASD.Block($decll.out, $instrl.out); }
    ;

conditional_instruction returns [TP2.ASD.Instruction out]
    : IF cond=expression
        THEN then_instr=instruction
        ELSE else_instr=instruction
      FI
      {$out = new TP2.ASD.IfInstruction($cond.out,$then_instr.out, $else_instr.out);}
    | IF cond=expression
        THEN then_instr=instruction
      FI
      {$out = new TP2.ASD.IfInstruction($cond.out,$then_instr.out, new TP2.ASD.EmptyInstruction());}

    ;

while_instruction returns [TP2.ASD.Instruction out]
    : WHILE cond=expression
        DO do_instr=instruction DONE
      {$out = new TP2.ASD.WhileInstruction($cond.out, $do_instr.out);}
    ;

return_instruction returns [TP2.ASD.Instruction out]
    : RETURN expr=expression {$out= new TP2.ASD.ReturnInstruction($expr.out);}
    ;

instruction returns [TP2.ASD.Instruction out]
   : aff=affectation {$out=$aff.out;}
   | if_instr = conditional_instruction {$out=$if_instr.out;}
   | while_instr = while_instruction {$out=$while_instr.out;}
   | ret_instr = return_instruction {$out=$ret_instr.out;}
   | func_instr = func_call_instruction {$out = $func_instr.out;}
   | print_instr = print_instruction {$out=$print_instr.out;}
   | read_instr = read_instruction {$out=$read_instr.out;}
   | OPENING_BRACKETS blck=block CLOSING_BRACKETS {$out=$blck.out;}
   ;

instruction_list returns [LinkedList<TP2.ASD.Instruction> out]
    : instr=instruction next_instrs=instruction_list {
        LinkedList<TP2.ASD.Instruction> instrl = $next_instrs.out;
        instrl.add($instr.out);
        $out=instrl;}
    | instr=instruction {
        LinkedList<TP2.ASD.Instruction> instrl = new LinkedList<TP2.ASD.Instruction>();
        instrl.add($instr.out);
        $out=instrl;}
    ;

//TODO CODE
declaration returns [TP2.ASD.DeclarationInstruction out]
    : type=var_type ident_list=identifiers_list {
        $out = new TP2.ASD.DeclarationInstruction($type.out,
                                                  $ident_list.out.getKey(),
                                                  $ident_list.out.getValue()); }
    ;

declaration_list returns[LinkedList<TP2.ASD.DeclarationInstruction> out]
    : decl=declaration next_decls=declaration_list {
        LinkedList<TP2.ASD.DeclarationInstruction> decll = $next_decls.out;
        decll.add($decl.out);
        $out=decll;
    }
    | {$out= new LinkedList<TP2.ASD.DeclarationInstruction>();}
    ;

function_declaration returns [TP2.ASD.FunctionDeclarationInstruction out]
    : PROTO ret_type=func_type id=IDENT
          LP args=function_arguments_list RP {
            $out = new TP2.ASD.FunctionDeclarationInstruction($ret_type.out, $id.text, $args.out);
            }
    ;

main_function_declaration returns [TP2.ASD.FunctionDeclarationInstruction out]
    : PROTO ret_type=func_type MAIN
          LP RP {
            LinkedList<String> args = new LinkedList<String>();
            $out = new TP2.ASD.FunctionDeclarationInstruction($ret_type.out, "main", args);
            }
    ;

identifier returns [String out]
    : ident=IDENT {$out = $ident.text;}
    ;

identifiers_list returns [Pair< List<String>, List<Pair<Integer, String>> > out]
    : id=identifier LSQB size=INTEGER RSQB COMMA next_ids=identifiers_list {
        Pair< List<String>, List<Pair<Integer, String>> > next_ids = $next_ids.out;
        List<String> idl = next_ids.getKey();
        List<Pair<Integer, String>> tidl = next_ids.getValue();
        tidl.add(new Utils.Pair($size.int,$id.out));
        $out=new Utils.Pair(idl,tidl);
    }
    |id=identifier LSQB size=INTEGER RSQB {
        List<String> idl = new LinkedList<String>();
        List<Pair<Integer, String>> tidl = new LinkedList<Pair<Integer, String>>();
        tidl.add(new Utils.Pair($size.int,$id.out));
        $out=new Utils.Pair(idl,tidl);
    }
    | id=identifier COMMA next_ids=identifiers_list {
        Pair< List<String>, List<Pair<Integer, String>> > next_ids = $next_ids.out;
        List<String> idl = next_ids.getKey();
        List<Pair<Integer, String>> tidl = next_ids.getValue();
        idl.add($id.out);
        $out=new Utils.Pair(idl,tidl);
      }
    | id=identifier {
        List<String> idl = new LinkedList<String>();
        List<Pair<Integer, String>> tidl = new LinkedList<Pair<Integer, String>>();
        idl.add($id.out);
        $out=new Utils.Pair(idl,tidl);
      }
    ;

affectation returns [TP2.ASD.Instruction out]
    : ident=IDENT AFFECT exp=expression
        {$out = new TP2.ASD.AffectationInstruction($ident.text, $exp.out);}
    | ident=IDENT LSQB index=expression RSQB AFFECT exp=expression
        {$out = new TP2.ASD.TableAffectationInstruction($ident.text, $index.out, $exp.out);}
    ;


// Handling read & print Here
read_instruction returns [TP2.ASD.Instruction out]
    : READ id=read_arguments_list {
          $out = new TP2.ASD.ReadInstruction($id.out);
      }
    ;

read_arguments_list returns [List<Object> out]
    : id=read_argument COMMA next_ids=read_arguments_list {
      List<Object> idl = $next_ids.out;
      idl.add($id.out);
      $out=idl;
      }
    | id=read_argument {
      List<Object> idl = new LinkedList<Object>();
      idl.add($id.out);
      $out=idl;
      }
    | {$out = new LinkedList<Object>();}
  ;

read_argument returns [Object out]
    : id=IDENT{
        $out = new String($id.text);
      }
    | id=IDENT LSQB expr=expression RSQB {
        $out = new Pair($id.text, $expr.out);
      }
    ;

print_instruction returns [TP2.ASD.Instruction out]
    : PRINT print_args=print_arguments_list {
          $out = new TP2.ASD.PrintInstruction($print_args.out);
      }
    ;

func_call_instruction returns [TP2.ASD.Instruction out]
    : id=IDENT LP exprl = expression_list RP {
        $out = new TP2.ASD.FunctionCallInstruction($id.text, $exprl.out);
      }
    ;

print_arguments_list returns [List<Object> out]
    : id=print_argument COMMA next_ids=print_arguments_list {
    //TODO CORRECT THIS! ONLY VARIABLES CONSIDERED!
      List<Object> idl = $next_ids.out;
      idl.add($id.out);
      $out=idl;
      }
    | id=print_argument {
      List<Object> idl = new LinkedList<Object>();
      idl.add($id.out);
      $out=idl;
      }
    | {$out = new LinkedList<Object>();}
    ;

print_argument returns [Object out]
  : str=TEXT {
      $out = new String($str.text);
    }
  | expr=expression {
      $out = $expr.out;
    }
  ;


/******************************************************************************/

expression returns [TP2.ASD.Expression out]
    : calc_expr = calculus_expression {$out = $calc_expr.out;}
    ;

calculus_expression returns [TP2.ASD.Expression out]
    : exp = addition_expression {$out = $exp.out;}
    ;

addition_expression returns [TP2.ASD.Expression out]
    : l=substraction_expression PLUS r=addition_expression
                            {$out = new TP2.ASD.AddExpression($l.out, $r.out);}
    | exp=substraction_expression {$out = $exp.out;}
    ;

substraction_expression returns [TP2.ASD.Expression out]
    : l=multiplication_expression MINUS r=consecutive_substraction_expression
                            {$out = new TP2.ASD.SubExpression($l.out, $r.out);}
    | exp=multiplication_expression {$out = $exp.out;}
    ;

consecutive_substraction_expression returns [TP2.ASD.Expression out]
    : l=multiplication_expression MINUS r=consecutive_substraction_expression
                            {$out = new TP2.ASD.AddExpression($l.out, $r.out );}
    | exp=multiplication_expression {$out = $exp.out;}
    ;

multiplication_expression returns [TP2.ASD.Expression out]
    : l=division_expression TIMES r=multiplication_expression {$out = new TP2.ASD.MultExpression($l.out, $r.out);}
    | f=division_expression {$out = $f.out;}
    ;

division_expression returns [TP2.ASD.Expression out]
    : l=factor DIVIDE r=consecutive_division_expression {$out = new TP2.ASD.DivExpression($l.out, $r.out);}
    | f=factor {$out = $f.out;}
    ;

consecutive_division_expression returns [TP2.ASD.Expression out]
    : l=multiplication_expression DIVIDE r=consecutive_division_expression
                            {$out = new TP2.ASD.MultExpression($l.out, $r.out );}
    | exp=multiplication_expression {$out = $exp.out;}
    ;

factor returns [TP2.ASD.Expression out]
    : p=primary { $out = $p.out; }
    // TODO : that's all?
    | LP exp=expression RP {$out = $exp.out;}
    | id=IDENT LSQB exp = expression RSQB
      {$out = new TP2.ASD.TableElementExpression($id.text, $exp.out); }
    | id=IDENT LP exprl = expression_list RP {
        $out = new TP2.ASD.FunctionCallExpression($id.text, $exprl.out);
    }
    ;

primary returns [TP2.ASD.Expression out]
    : INTEGER { $out = new TP2.ASD.IntegerExpression($INTEGER.int); }
    // TODO : that's all?
    | IDENT {$out = new TP2.ASD.IdentExpression($IDENT.text);}
    ;

var_type returns [TP2.ASD.Type out]
    : INT {$out = new TP2.ASD.Int();}
    ;

void_type returns [TP2.ASD.Type out]
    : VOID {$out = new TP2.ASD.Void();}
    ;

func_type returns [TP2.ASD.Type out]
    : var_ret_type=var_type {$out = $var_ret_type.out;}
    | void_ret_type=void_type {$out = $void_ret_type.out;}
    ;

expression_list returns [List<TP2.ASD.Expression> out]
    : exp = expression COMMA exprl=expression_list {
          List<TP2.ASD.Expression> expressions = $exprl.out;
          expressions.add($exp.out);
          $out = expressions;
        }
    | exp = expression {
        List<TP2.ASD.Expression> expressions = new LinkedList<TP2.ASD.Expression>();
        expressions.add($exp.out);
        $out = expressions;
      }
    | {$out = new LinkedList<TP2.ASD.Expression>();}
    ;

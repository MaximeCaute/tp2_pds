lexer grammar VSLLexer;

options {
  language = Java;
}

@header {
  package TP2;
}

WS : (' '|'\n'|'\t') -> skip
   ;

COMMENT : '//' (~'\n')* -> skip
        ;

fragment LETTER : 'a'..'z' ;
fragment DIGIT  : '0'..'9' ;
fragment ASCII  : ~('\n'|'"');

// keywords
LP    : '(' ; // Left parenthesis
RP    : ')' ;
LSQB  : '[' ;
RSQB  : ']' ;
PLUS  : '+' ;

// other tokens (no conflict with keywords in VSL)
IDENT   : LETTER (LETTER|DIGIT)*;
TEXT    : '"' (ASCII)* '"' { setText(getText().substring(1, getText().length() - 1)); };
INTEGER : (DIGIT)+ ;

// fall below, fails otherwise (?_?)
PRINT : 'PRINT' ;
READ  : 'READ'  ;

COMMA : ',' ;
//QUOTE : '"' ;

MINUS : '-' ;
TIMES : '*' ;
DIVIDE: '/' ;
AFFECT: ':=';

OPENING_BRACKETS: '{' ;
CLOSING_BRACKETS: '}' ;

// textual keywords
INT   : 'INT'   ;

VOID  : 'VOID'  ;
FUNC  : 'FUNC'  ;
RETURN: 'RETURN';

WHILE : 'WHILE' ;
DONE  : 'DONE'  ;
DO    : 'DO'    ;

IF    : 'IF'    ;
THEN  : 'THEN'  ;
ELSE  : 'ELSE'  ;
FI    : 'FI'    ;

MAIN  : 'main';
PROTO : 'PROTO';
